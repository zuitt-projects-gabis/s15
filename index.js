let name1 = "Juan";
let name2 = "Shane";
let name3 = "Peter";
let name4 = "Jack";

let number = "50";
let number2 = "60";
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number);
	// result:true
console.log(numString1 === number);
	//result: false
console.log(numString1 != number);
	//result: false
console.log(name4 !== name3);
	//result: true
console.log(name1 == "juan");
	//result: false
console.log(name1 === "Juan");
	//result: true	

//Relational Comparison Operators
	// A comparison operator which will check the relationship between the operands

	let q = 500;
	let r = 700;
	let w = 8000;
	let numString3 = "5500";

	//Greater Than (>)
	console.log(q > r);
		//result: false
	console.log(w > r);
		//result: true
	
	//Less Than (<)
	console.log(w < q);
		//result: false
	console.log(q < 1000);
		//result: true
	console.log(numString3 < 6000);
		//result: true
	console.log(numString3 < "Jose");
		//result: true - this is erratic

	//Greater than or equal to (>=)
	
	console.log(w >= 8000);
		//result: true
	console.log(r >= q);
		//result: true

	// Less than or equal to (<=)

		console.log(q <= r);
			// result: true
		console.log(w <= q);
		
	//Logical Operators
		// and Operato (&&)
			//Both operands on the left and right or all operands added must be true or will result to true

	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;




	let authorization1 = isAdmin && isRegistered;
	console.log(authorization1);
		//result: false
	let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2);

	let authorization3 = isRegistered && requiredLevel === 25;
	console.log(authorization3);

	let authorization4 = isRegistered && isLegalAge && requiredLevel == 95;
	console.log(authorization4);

	let registration1 = userName.length > 8 && userAge >= requiredAge;
	console.log(registration1);

	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
	console.log(registration2);

		//OR operator ( || - double pipe)
			//OR operator returns true if at least one of the operands are true
	let userLevel = 100;
	let userLevel2 = 65;

	let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
	console.log(guildRequirement1);

	let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
	console.log(guildRequirement2);		

	let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin);

		//Not Operator (!)
			// turns a boolean value into the opposite value

		console.log(!isRegistered);
			// result: false
		console.log(!guildAdmin);
			// result: 

// If-else statements
		// if statement will run a block of code if the condition specified is true or results to true

		//if(true){
		//	alert('We run an if condition!')
		//}

	let userName3 = "crusader_1993";
	let userLevel3 = 25;
	let userAge3 = 20;

	if(userName3.length > 10 ){
		console.log("Welcome to Game Online!")
	};

	if(userLevel3 >= requiredLevel){
		console.log("You are qualified to join the guild!");
	};

	if(userName.length >= 10 && isRegistered && isAdmin){
		console.log("Thank you for joining the admin!")
	};

	// else statement
		// will run if the condition given is false or  results to fasle

	if (userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
		console.log("Thank you for joining the Noob guild!") 
	}
	else{
		console.log("You are too strong to be a noob: (")
	};

	// else if
		// executes a statement if the previous or the original condition is false or resulted to flase but another specified condition resulted to true

	if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
		console.log("Thank you for joining the Noob guild");
	} else if (userLevel3 >= 25){
		console.log("You are too strong to be a noob")
	} else if (userAge3 < requiredAge) {
		console.log("You are too young to join the guild");
	} else if (userName3.length < 10){
		console.log("Username is too short")
	} else {
		console.log("Unable to join.")
	};

		// if-else inside a function

		function addNum(num1, num2){


			//typeof checks the data type
			// check if the numbers being passed are number types
			if(typeof num1 === "number" && typeof num2 === "number"){

				console.log("Run only if both arguments passed are number types")
			} else {
				console.log("One or both of the arguments are not numbers");	
			};	
		};

	addNum(5 ,2);

		function login(username, password){
			if(typeof username === "string" && typeof password ==== "string"){
				console.log("Both arguments are string");

				//nested if-else statement
			if(username.length >= 8 && password.legnth >= 8  ){
				alert("Thank you for logging in"){
				else if(username.length < 8){
				alert("username is too short")
				else if(password.length < 8){
				alert("password is too short")	
					}

				} else {
					console.log("One of the arguments is not string type")
				}
		};
		
		login("theThinker", "tinkerbell");

// Switch Statements
	// is an alternative to an if, else-if, else tree, where the data being evaluated or checked is of an expected input.

	//Syntax:
		/*
			switch(expression/condition){
				case value:
					statement;
					break;
				default:
					statement;
					break;	
			}
		*/				
	let hero = "Hercules";

	switch(hero){
		case "Jose Rizal":
		console.log("National Hero of the Philippines");
		break;

		case "George Washington":
		console.log("Hero of the American Revolution");
		break;

		case "Hercules":
		case.log("Legendary hero of the Greeks");
		break;
	};

		function roleChecker(role){

			switch(role){
				case "Admin":
					console.log("Welcome admin!");
					break;

				case "User":
					console.log("Welcome User");
					break;

				case "Guest":
				console.log("Welcome Guest!");		
					break;

				default:
					console.log("Invalid role");	


			}

		}


		roleChecker("Pikachu");